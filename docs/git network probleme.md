# Problem "klonen auf HeimLW WIFI Wien"
## Lösung 1
- leeren Ordner anlegen
- zu Workspace hinzufügen
- Terminal öffnen
- git init --initial-branch=main
- git remote add origin https://gitlab.com/<<<EIGENERPFAD>>>
- dann in SCM in VSC wechseln
- origin/main Branch wählen
- Synchronisieren

## Lösung 2
- Repo auf C: klonen
- Ordner auf Heimlaufwerk verschieben
- zu Workspace hinzufügen

# erstes Mail GIT User & Email global eintragen (Commit geht sonst nicht)
- git config --global user.name "..."
- git config --global user.email "..." 

# GIT Passwort zurücksetzen
- git config --global --unset user.password
