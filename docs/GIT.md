# GIT

- Account bei gitlab.com 
- create blank project (Projektname, Private, mit README.md initialisieren)
- Button "Clone", HTTPS Link kopieren
- in VSC: Command Palette (STRG+SHIFT+P)
- GIT:clone 
    * wenn nicht vorhanden, GIT installieren und VSC neu starten (git-scm.com)
- GIT URL einfügen
- beim 1. Mal GIT Credentials (Username + Passwort) eingeben
    * oder direkt in Clone-URL HTTP-Authentication einfügen https://USERNAME:PASSWORT@...
- lokalen Ordner auswählen
- Ordner zum aktuellen Arbeitsbereich (Workspace) hinzufügen 
    * oder im Menü Datei/Ordner zum Arbeitsbereich hinzufügen

WICHTIG:
- nicht in ein bestehendes Repository klonen, immer ein neuer Ordner!

# erstes Mal GIT User & Email global eintragen (Commit geht sonst nicht)
- git config --global user.name "..."
- git config --global user.email "..." 