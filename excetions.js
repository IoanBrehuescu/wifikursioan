try {

    console.log('Beginn des Try-Laufs');

    collision;

    console.log('Ende des Try-Laufs -- niemals erreicht');

} catch (err) {

    console.log('Ein Fehler ist aufgetreten! ' + err) // .stack !!! .message, .name

} finally {
    console.log('Dies läuft immer!')
}

console.log('...Dann geht die Ausführung weiter')



let json = '{"name":"Ioan"}';

try {

    let user = JSON.parse(json);
    if (!user.alter)
        throw new SyntaxError("Unvollständige Daten: kein Alter")

    console.log(user.alter);

} catch (e) {

    console.log('JSON Error ' + e ) // .stack !!! .message, .name

}

