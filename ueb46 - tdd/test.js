QUnit.test( 'Multiplikation 2', ( assert )=>{
   // assert.ok( x>5, '...' ); // p1 => true, TEST erfolgreich
   // assert.equal( p1, p2, '...' ); // wenn p1 gleich p2 ist dann TEST erfolgreich
   assert.ok( typeof multiplikation2 == 'function', 'Funktion multiplikation2() ist vorhanden.' );
   assert.equal( multiplikation2(4), 8, 'multiplikation2(4) liefert 8' );
   assert.equal( multiplikation2(5), 10, 'multiplikation2(5) liefert 10' );
   assert.equal( multiplikation2(57), 114, 'multiplikation2(57) liefert 114' );
});

QUnit.test( 'Addiere zwei Zahlen', function(assert) {
    assert.equal( typeof add, 'function', 'add() existiert' );
    assert.equal( add(2,3), 5, 'Funktion addiert zwei Zahlen' );
    assert.equal( add('2','3'), 5, 'Addiere mit Typumwandung' );
    assert.equal( add('a',5), 0, 'Buchstaben übergeben liefert 0' );
    assert.equal( add(5,'b'), 0, 'Buchstaben übergeben liefert 0' );
    assert.equal( add( 1.5, 1), 2.5, 'Addieren mit Kommazahlen' );
    assert.equal( add( '1,5', '2,2' ), 3.7, 'Addieren mit Kommza als Beistrich' );
    assert.equal( add(1), 0, 'Fehlende Parameter Ergebnis ist 0' );
    assert.equal( add(5,'3'), 8, 'Zalh und String auch addieren' );
    assert.equal( add(), 0, 'gar kein Wert liefert 0' );
});


// asynchrone Tests
// Callback, keine return
// Ziel: Komponente, die zwei Zahlen addiert (aber asynchron! länger als 1s)
// asyncAdd(p1,p2,callback)

QUnit.test( 'Asynchrones Addieren', (assert)=>{
    assert.notEqual( asyncAdd(2,3), 5, 'kein Ergebnis weil asynchron!' );
    // assert.timeout( 30000 );
    let done = assert.async(); // warte bis done() aufgerufen wird
    asyncAdd(2,3, (erg)=>{ 
        assert.equal( erg, 5, 'Addition asnchron mit Callback' );
        done();
    });
});

// SETUP/TEARDOWN
// Ziel: Komponente addiere 2 Zahlen und schreib Ergebnis in den div#ausgabe

QUnit.module( 'Ausgabe in HTML', {
    before:()=>{ // SETUP
        let div = document.createElement('div');
        div.id = 'ausgabe';
        document.querySelector( '#qunit-fixture' ).appendChild( div );
    },
    after:()=>{ // TEARDOWN
        document.querySelector( '#qunit-fixture' ).innerHTML = '';
    }
});

QUnit.test( 'Addieren mit Ausgabe', (assert)=>{
    assert.equal( typeof addHTML, 'function', 'addHTML() existiert' );
    addHTML(5,7);
    assert.equal( document.querySelector( '#ausgabe' ).innerHTML, '12', 'Ausgabe Summe passt.'  );
});

