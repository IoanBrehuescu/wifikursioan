# API - Quiz
Schnittstellen für Javascript-Quiz

## Anzahl der Fragen am Server
### Request
URL: http://wifi.1av.at/quiz.php
Method: POST
Data: 
- type: 'getcount'
### Response
Format: JSON
Data:
- max: INTEGER 

## Hol eine Frage
### Request
URL: http://wifi.1av.at/quiz.php
Method: POST
Data:
- type: 'getquestion',
- id: INTEGER 

send( 'type=getquestion&id=1' )
send( '{"type":"getquestion","id":1}' )


### Response
Format: JSON
Data:
- text: STRING
- antworten: ARRAY mit STRING

## Überprüfe Antwort
### Request
URL: http://wifi.1av.at/quiz.php
Method: POST
Format: application/x-www-form-urlencoded
Data:
- type: 'check'
- id: INTEGER 
### Response
Format: JSON
Data:
- correct: INTEGER [0-3]

## Highscore 
### Request
URL: http://wifi.1av.at/quiz.php
Method: POST
Data:
- type: 'highscore'
- name: STRING
- points: INTEGER (1-100000)
### Response
Format: JSON
Data:
- highscore: Array


======

## ERROR-Response
Data:
- error: INTEGER

### Error-Codes
  404: Frage gibt es nicht
  500: Es stimmt was nicht
