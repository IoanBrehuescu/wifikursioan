// Resolv yaml-main problem
Dacă vrei să combini modificările de la distanță cu cele locale (merge), rulează:

git pull --rebase=false origin main

Dacă vrei să aplici modificările tale peste cele de la distanță (rebase), rulează:

git pull --rebase=true origin main

Dacă vrei doar să aduci modificările de la distanță și să ignori cele locale, rulează:

git fetch origin
git reset --hard origin/main






#  Ausbildung zum geprüften Software Developer Javascript
## M1. HTML/CSS für Webdeveloper
--- 
### Tag 1 (25.9.2023, C302)
- Kennenlernen & Organisatorisches
- GIT Repo einrichten
- HTML Syntax und Semantik
  * Strukturierung von Dokumenten
  * Block/Inline Elemente
  * Hyperlinks
  * Pfade (rel., abs.)  

#### Übungen
- UEB1 (Mini-Webseite Thema HTTP)

#### Hyperlinks
- [VisualStudioCode](https://code.visualstudio.com/)
- [Microsoft Teams](https://www.microsoft.com/en-ww/microsoft-365/microsoft-teams/download-app#desktopAppDownloadregion)
- [W3Schools](https://www.w3schools.com/)
- [SELFHTML](https://wiki.selfhtml.org/)
- [HTML5 Referenz](https://developer.mozilla.org/en-US/docs/Web/HTML)
- [EMMET Doku](https://docs.emmet.io/abbreviations/syntax/)
---
### Tag 2 (26.9.2023, C302)
- UX, UI Design mit Hrn. Halm
- Mural
- FIGMA
---
### Tag 3 (2.10.2023, C302)
- HTML
  * Bilder
  * Pfade (rel., abs., etc.)  
- CSS
  * Basics
  * Textformatierung
  * Selektoren (Tag, Klassen, ID, Pseudoklassen, kombinieren)
  * Einheiten
  * Spezifität
  * Box-Modell  
  * Positionieren (Basics)

#### Übungen
- UEB2 (Inserat)

#### Hyperlinks
- [Pagespeed](https://developers.google.com/speed/pagespeed/insights/)
- [W3C Validator](https://validator.w3.org/)
- [CanIUse](https://caniuse.com/)
- [CSS Selektoren](https://flukeout.github.io/)
---
### Tag 4 (3.10.2023, C302)
- HTML
  * Tabelle
  * Formular  
- CSS
  * Shapes
  * Positionieren
  * Font
  * Pseudoelemente  

#### Übungen
- UEB3 (diverse Inserate)
- UEB4 (Benutzer-Tabelle)

#### Hyperlinks
- [Google Fonts](https://fonts.google.com/)
- [Google Fonts Selfhosting](https://gwfh.mranftl.com/fonts)
- [Colorpicker](https://pickcoloronline.com/)
- [iPhone in CSS](https://www.zubach.com/iphone)
- [IcoMoon - free Icons](https://icomoon.io/)
- [FlatIcon](https://flaticon.com/)
- [CSS Triangle](https://triangle.designyourcode.io/)
- [FontFace](https://developer.mozilla.org/en-US/docs/Web/CSS/@font-face)
---
### Tag 5 (9.10.2023, C302)
- HTML 
  * Responsive Seiten
  * Formular
  * Navigation
- CSS  
  * Hintergrundbilder
  * Float 
  * Flexbox
  * Mediaqueries
  * Sichtbarkeit

#### Hyperlinks
- [Gradient Generator](https://www.colorzilla.com/gradient-editor/)
- [Codrops](https://tympanus.net/codrops/)
- [Flexbox lernen](https://flexboxfroggy.com/)
- [Flexbox defense](http://www.flexboxdefense.com/)
- [Flexbox Guide](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
- [Learn CSS Games](https://codingfantasy.com/games)
- [GridGarden](https://cssgridgarden.com/)
- [CSS Menus](http://www.cssplay.co.uk/menus/)


