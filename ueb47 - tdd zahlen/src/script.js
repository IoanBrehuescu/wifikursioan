

function add() {
    var a = document.querySelector('#z1').value * 1;
    var b = document.querySelector('#z2').value * 1;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'http://wifiweb.bplaced.net/summe.php');
    xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if (xhr.readyState === 4 && xhr.status === 200) {
            try {
                let jsonResponse = JSON.parse(xhr.responseText);
                document.querySelector('#ausgabe').innerHTML = jsonResponse.summe;
            } catch(e) {
                console.error('Fehler beim Parsen der Antwort:', e);
            }
        }
    };

    xhr.send('a=' + a + '&b=' + b);
}
document.querySelector('#btn1').addEventListener('click', function(event) {
    event.preventDefault(); 
   add();
});
