let fs = require('fs');// - Node Standard Module (nicht installieren!!!)

//Datei schreiben

fs.writeFile(__dirname + '/data/test.txt', 'Hallo Datei!', (err)=>{
  if(!err){
    console.log('text.txt fertig')
  }else{
    console.log('Error beim Schreiben von test.txt')
  }
});

//Datei einlesen
fs.readFile( __dirname + '/data/test.txt',(err,data)=>{
if(!err){
    console.log(data.toString() );
}
if (data == 'Hallo Datei!'){
    console.log('Ok!')
}
})

fs.readFile( __dirname + '/data/test.json',(err,data)=>{
    if(!err){
        console.log(data);
        console.log(data.toString() );// stringifizierter JSON
        let o = JSON.parse(data.toString() );
        console.log(o); //Objekt
    }else{
        console.log('ERROR')
    }
   
    })
    // fs Funktionen als Promises

    fs.promises.readFile(__dirname + '/data/test.txt').then(data => {
        console.log('PROMISES',data.toString() );
    }).catch(()=>{console.log('ERROR')})

    let readText = async() =>{
        try{ let data = await fs.promises.readFile(__dirname + '/data/test.txt');
        console.log('Promises await',data)}
       catch{
        console.log('ERROR')
       }
    }
    readText();