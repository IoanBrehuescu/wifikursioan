// wenn sich die Datei server.js ändert, dann soll Program beendet werden und neu starten


//Datei => fs
//Program aus Program starten => Program = Prozess => child_procces

let fs = require('fs');
let cp = require('child_process')

console.log('Monitor gestartet')

// wenn Mnitor gestartet wirdd, starte Server

let server = cp.fork('server.js')

fs.watchFile('server.js', ()=>{
    console.log('server.js hat sich geändert');
    server.kill(); // beendet Prozess
    console.log('server.js beendet')
    server = cp.fork('server.js')
})