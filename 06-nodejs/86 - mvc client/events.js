var MVC = (function( mvc ) {

    mvc._Event = function( sender ) {
        this._sender = sender;
        this._listeners = [];

        this.on = function( listener ) {
            this._listeners.push( listener );
        }

        this.emit = function( args ) {
            for ( let i in this._listeners ) {
                this._listeners[i]( this._sender, args );
            }
        }
    }    

    return mvc;

}( MVC || {} ) )
