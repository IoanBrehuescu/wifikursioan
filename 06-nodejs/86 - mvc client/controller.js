var MVC = (function (mvc) {

    mvc.Controller = function (model, view) {
        this._model = model;
        this._view = view;

        this._view.clickButton.on(() => {
            this._model.add(1)
        })

    }


    return mvc;

})(MVC || {});    