var MVC = (function (mvc) {

    mvc.Model = function (data) {
        this._data = data;
        //event on set
        this.onSet = new mvc._Event(this);
        this.onSet.on(function (sender, data) {
            console.log('neuer Wert', data);
        })
        //daten abruffen
        this.get = function () {
            return this._data;
        }
        // ende ich die daten mit ein neuen wert
        this.set = function (data) {
            if(data <= 10){
                this._data = data;
                this.onSet.emit(data);
            }
            
        }

        this.add = function (z) {
            // nr = this.get() + z
            this.set(this.get() + z)
        }

    }


    return mvc;
}(MVC || {}));  