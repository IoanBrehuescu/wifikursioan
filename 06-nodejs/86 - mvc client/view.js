var MVC = (function (mvc) {

    mvc.View = function (model, elements) {
        // view - span
        this._model = model;
        this._span = elements[0];
        this._button = elements[1];

        this.show = function () {
            this._span.innerHTML = this._model.get();
        }
        // wenn sich the wert endert 
        this._model.onSet.on(this.show.bind(this));
        this.clickButton = new mvc._Event(this);

        this._button.addEventListener('click', () => {
            this.clickButton.emit();
        });
    }

    return mvc;
}(MVC || {}));  