# Vokabeltrainer


## Backend
- Verwaltung
- CRUD (create read update delete)
- RESTful API
- URL http://localhost:8001/vokabel
- Methode GET => read
- Methode POST => create
- Methode PUT => update komplette Daten
- Methode PATCH => update nur Daten ändern, die mitgeschickt werden
- Methode DELETE => delete

- GET http://localhost:8001/vokabel => Alle Daten
- GET http://localhost:8001/vokabel/ID => Daten nur für ID
- POST http://localhost:8001/vokabel => Body Daten mitschiken (komplett ohne ID)
- PUT http://localhost:8001/vokabel/ID => Body allen Daten mitschicken
- DELETE http://localhost:8001/vokabel/ID => ID wird gelöscht
- Response Status => 200, 404, 407, ...
- UI (Authentication)

## Frontend
- Vokabel UI Daten nutzt