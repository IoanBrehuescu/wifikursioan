const express = require('express');
const fs = require('fs');
const { v4: uuidv4 } = require('uuid')
const session = require('express-session')
const app = express()
const PORT = process.env.PORT || 8001

app.listen(PORT, () => { console.log(`Express http://localhost:${PORT}`) });

//Middleware session mit UI
app.set('trust proxy', 1);
app.use(session({
    secret:'abcdefghijklmnopq',
    resave: false,
    saveUninitialized:true,
    cookie:{secure:false}
}) );

app.use('/admin',(req,res,next)=>{
    if(req.session.eingelogt){
        next();
    }else{
        res.status(401).end( 'nicht eingeloggt' )
    }
})


app.use(express.static('public')); // Request / => index.html
app.use(express.urlencoded({ extended: false }));//Post BOdy Daten einlessen(x-www-form-urlencoded#)

app.use(express.json());//(PPLICtion/json)

// Routing
/*
app.get( '/',(req,res)=>{
    res.sendFile(__dirname + '/public/index.html' )
})
*/

const VOCFILE = 'data/vokabel.json';
const USERFILE = 'data/user.json'

// CRUD Restful Vokabel
app.get('/vokabel', (req, res) => {
    console.log('read all');
    fs.readFile(VOCFILE, (err, data) => {
        let voc = JSON.parse(data)
        res.send(voc)
    })

});

app.get('/vokabel/:id', async (req, res) => {
    console.log('read', req.params.id);
    fs.readFile(VOCFILE, (err, data) => {
        if (!err) {
            let voc = JSON.parse(data)
            let gefunden = false
            for (let i = 0; i < voc.length; i++) {
                if (voc[i].id == req.params.id) {
                    res.send(voc[i]);
                    gefunden = true;
                }
            }

            if (!gefunden) {
                res.status(404).end('')
            }
        } else {
            res.status(500).end('')
        }

    });
});


app.post('/vokabel', async (req, res) => {
    console.log('create');
    let voc = JSON.parse(await fs.promises.readFile(VOCFILE));
    let neueVoc = {
        id: uuidv4(),
        de: req.body.de, //WICHTIG Bodyparser
        en: req.body.en
    };
    voc.push(neueVoc);

    await fs.promises.writeFile(VOCFILE, JSON.stringify(voc, null, "\t"));
    res.end('create');
});
app.put('/vokabel/:id', async (req, res) => { // alle Daten auser ID
    console.log('update', req.params.id);
    let voc = JSON.parse(await fs.promises.readFile(VOCFILE));
    // Eintrag


    let gefunden = false
    for (let i = 0; i < voc.length; i++) {
        console.log(voc[i].id, req.params.id)
        if (voc[i].id == req.params.id) {
            voc[i].de = req.body.de
            voc[i].en = req.body.en
            gefunden = true;
            console.log(voc[i])
        }

    }
    console.log(voc)
    await fs.promises.writeFile(VOCFILE, JSON.stringify(voc, null, "\t"));
    if (!gefunden) {
        res.status(404).end('')
    } else {
        res.end('update')
    }
});

app.patch('/vokabel/:id', (req, res) => {
    console.log('update', req.params.id);
    res.end('update');
});

// app.delete('/vokabel/:id', async(req, res) => { // :id => URL Parameter findet man in req.params
//     console.log('delete', req.params.id);

//     let voc = JSON.parse(await fs.promises.readFile(VOCFILE));
//     // Eintrag


//     let gefunden = false
//     for (let i = 0; i < voc.length; i++) {
//         console.log(voc[i].id, req.params.id)
//         if (voc[i].id == req.params.id) {
//             voc[i].splice[i,1]
//             gefunden = true;
//             console.log(voc[i])
//         }

//     }

//     await fs.promises.writeFile(VOCFILE, JSON.stringify(voc, null, "\t"));
//     if (!gefunden) {
//         res.status(404).end('')
//     } else {
//         res.end('delete');
//     }


// });


app.post('/login', async (req, res) => {
    let users = JSON.parse(await fs.promises.readFile(USERFILE));
    let gefunden = false
    for (let i = 0; i < users.length; i++) {
        if (users[i].username == req.body.username && users[i].passwort == req.body.passwort) {
            gefunden = true;
        }
    }
    if (gefunden) {

        //merken in Session, dass User eingeloggt ist
        req.session.eingeloggt = req.body.username; // in Session gespeichert (am Server!)
        res.end('gefunden')
    } else {
        res.status(401).end('')
    }
});
app.get('/logout',(req,res)=>{
    delete req.session.eingeloggt; // session wird nicht zerstört, nur der Wert eingeloggt wird entfernt
    res.redirect('/login.html');
});
app.get('/getusername')
