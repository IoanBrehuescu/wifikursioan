const http = require( 'http' ); // HTTP Server, Requests horchen und antworten
const dns = require( 'dns' ); // dns.resolve( 'xyz.com', 'MX', (err)=>{ } ); 
const fs = require( 'fs' ); // Dateisystem
const regEx = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;

if(email.match(regEx)){
    res.end('{}')
}
       


const anfrage = ( req, res )=>{
    console.log( 'Request', req.method, req.url );
    //res.writeHead(200,{'Access-Control-Allow-Origin':'*'});

    // Query-Parameter einlesen
    // /email?email=aaaaa <= STRING
    let urlteile = req.url.split( '?' );
    req.url = urlteile[0];

    // Routing
    if ( req.url == '/' ) {
        // Inhalt der HTML Datei
        fs.readFile( 'public/index.html', (err,data)=>{
            res.end( data );
        });
    } else if ( req.url == '/bootstrap' ) {
        fs.readFile( 'node_modules/bootstrap/dist/css/bootstrap.min.css', (err,data)=>{
            res.end( data );
        });
    } else if ( req.url == '/email' ) {
        // email?? 
        let parameter = urlteile[1]; 
        let email = parameter.split( '=' )[1];


        // CHECK DNS 
        // Check Syntax (RegExp)
        /*

        
        */

        if ( email.indexOf( '@' ) > -1 ) {
            res.end( '{"check":true}' );
        } else {
            res.end( JSON.stringify({check:false}) );
        }

       
    } else {
        res.end( 'TEST' ); // nur 1x, immer STRING (!)
    }
}


http.createServer( anfrage ).listen(26893); //5001-65000, keine Standardports, Port frei ist
console.log( 'Server gestartet. http://localhost:26893' );