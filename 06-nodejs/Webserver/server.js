let http = require( 'http' );
let fs = require( 'fs' );


let requestCB = (req,res)=>{

    if ( req.url == '/' ) {
        req.url = '/index.html';
    }

    if ( req.url == '/zufall' ) {
        req.url = '/zufall.html';
        let z = Math.floor(Math.random()*6)+1
        res.end( z.toString() );
    } else {
        fs.readFile( __dirname+req.url, (err,data)=>{
            if ( !err ) {
                res.writeHead(200);
                res.end( data );
            } else {
                res.writeHead( 404 );
                res.end( '' );
            }
        });
    }

   
}

http.createServer( requestCB ).listen( 6001 );
console.log( 'Server running on http://localhost:6001' );