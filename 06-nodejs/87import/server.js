// require => NodeJS nicht ECMAScript

const fs = require('fs')//bereits instaliert mit Node
const rn = require('random-number') //NPM Modul mit npm install randome-number
const zufall = require('./modules/zufall');//eigene mit ./ ausgehend Projekt-ROOT
const zufall2 = require('./modules/zufall2');// ganze Objekt in zufall2
const {random}=require('./modules/zufall2');//const randome = zufall2.random
const {random:rn2}=require('./modules/zufall2');//const rn2 = zufall2.random

//.js kann man weglassen => ./zufall.js = ./zufall
// index.js muss nicht geschrieben = ./zufall.js/index.js  => zufall

console.log(rn({min:1,max:10,integer:true}))
console.log(zufall(1,10))
console.log(zufall2.zufall(1,10));
console.log(zufall2.random(1,10));
console.log(random(1,10))