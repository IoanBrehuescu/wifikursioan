// auch require von anderen Modulen möglich!

const zufall = (a, b) => {
    return Math.floor(Math.random() * (b - a + 1)) + a;
}
module.exports = zufall;