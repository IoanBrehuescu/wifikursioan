const express = require( 'express' )

const app = express();
const PORT = 7008
const server = app.listen(7008, function() {
    console.log("Express läuft http://localhost:"+PORT);
  });

  let todos = [
    {text:'Aufgabe1',erledigt:false},
    {text:'Aufgabe2',erledigt:false},
    {text:'Aufgabe3',erledigt:true},
    {text:'Aufgabe4',erledigt:false}
  ]
  app.use(express.static("public"));
  app.use(express.urlencoded({extended:false}));


  app.get('/todos', (req, res) => {
    res.send(todos); 
    
});
app.post( '/neuestodo',(req,res)=>{
    let neuesTodo = {
        text:req.body.text,
        erledigt:false
    }
    todos.push( neuesTodo);
    res.end('ok')
} )
app.post('/checktodo/:index', (req, res) => {
    const index = req.params.index;
    if (index >= 0 && index < todos.length) {
        todos[index].erledigt = !todos[index].erledigt;
        res.end('Status updated');
    } else {
        res.status(404).end('Todo not found');
    }
});



//   app.get('/',(req,res)=>{
//     res.sendFile(__dirname+'/public/index.html');
//     //res.end('TEST')
//   })

//   app.get('/style.css',(req,res)=>{
//     res.sendFile(__dirname+'/public/style.css')
//   });