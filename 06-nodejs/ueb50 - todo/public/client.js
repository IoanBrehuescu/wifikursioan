function fetchTodos() {
    fetch('/todos')
        .then(response => {
            if (!response.ok) {
                response.end('Network response was not ok');
            }
            return response.json();
        })
        .then(todos => {
            updateTodoList(todos);
        })

}

function updateTodoList(todos) {
    const ul = document.getElementById('myUL');
    ul.innerHTML = '';

    todos.forEach((todo, index) => {
        const li = document.createElement('li');
        li.textContent = todo.text;

        li.addEventListener('click', () => {
            toggleStatus(index)
        });
        if (todo.erledigt) {
            li.classList.add('checked');
        }

        let span = document.createElement('span');
        span.classList.add('close')
        span.innerHTML = "x"
        li.appendChild(span)

        if (todo.erledigt) {
            li.classList.add('checked');
        }

        ul.appendChild(li);
    });

}
const toggleStatus = async (index)=>{
    await fetch(`/checktodo/${index}`, {
        method: 'POST'
    });
    fetchTodos();
}


const neuesTodo = async () => {
    let text = document.querySelector('#myInput').value.trim();
    if (text != '') {
        await fetch('/neuestodo', {
            method: 'post',
            headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
            body: `text=${text}`
        })
        fetchTodos();
    }
}
document.querySelector('.addBtn').addEventListener('click', neuesTodo)
fetchTodos();
