const express = require('express');
const socket = require('socket.io');


const app = express();
const PORT = process.env.PORT || 7007;

const server = app.listen(PORT, () => { console.log(`Chat server http://localhost:${PORT}`) })
const io = socket(server);

//Middleware HTTP-Request/XHR
app.use(express.static('public'));
app.use(express.static('node_modules/socket.io/client-dist'));// Socket



app.use(express.static('public')); // Request / => index.html

// POST Body Daten einlesen (x-www-form-urlencoded)



//Routing

// Sockets
io.on('connection', (sock) => { // sock => Tunnel zum Client
    console.log('neuer Client verbunden', sock.id);

    let username = '';

    sock.on('neueruser', (user) => {
        console.log('neuer User', user)
        username = user;
        io.emit('schreibe', `neuer User ${user}`)
    });
    sock.on('neuennachricht', (nachricht) => {
        io.emit('schreibe', `<b>${username}</b>: ${nachricht}`)
    })



    sock.on('disconnect', () => {
        console.log('User weg', sock.id)
        sock.brodcast.emit('schreibe', `<b>${username}</b>: hat den Chat verlassen`)
    });



});

// Session