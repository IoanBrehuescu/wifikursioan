const schreibe = (nachricht) => {
    document.querySelector('ul').innerHTML +=
        ` <li>${nachricht}</li>`
}
const changeUI = () => {
    
    document.querySelector('#m').setAttribute('placeholder', 'Schreibe eine Nachricht')
    document.querySelector('button').innerHTML = 'OK'
    document.querySelector('button').onclick = schreibeHandler;
    
}
let sock;
const schreibeHandler = (e) => {
    e.preventDefault();
    let nachricht = document.querySelector('#m').value;
    sock.emit('neuennachricht', nachricht);
    console.log(nachricht)
   // Clear the input field and reset any other properties
   let inputField = document.querySelector('#m');
   inputField.value = '';
   // Add here any other property you want to reset. For example:
   // inputField.placeholder = 'Schreibe eine Nachricht'; // if you want to reset the placeholder
   
   // Optionally, you can set the focus back to the input field
   inputField.focus();

}

const loginHandler = (e) => {
    e.preventDefault()
    let user = document.querySelector('#m').value
    if (user != '') {
        sock = io('https://chatioan.onrender.com/')//Verbindung
        sock.emit('neueruser', user)//schicke Server "user"
        sock.on('schreibe', schreibe)
        changeUI();
    }
}
document.querySelector('button').onclick = loginHandler