//require('dotenv).config()

const express = require('express');
const usermanager = require('./usermanager'); //index.js
const dotenv = require('dotenv')
const { v4: uuidv4 } = require('uuid');
const app = express();
const PORT = process.env.PORT || 5001;
const server = app.listen(PORT, () => { console.log(`${process.env.HOST}:${PORT}`) });


app.use(express.urlencoded({ extended: false }))// POST Requests!!!

app.use('/usermanager', usermanager);

app.get('/', (req, res) => {
    res.redirect('/usermanager')
})
app.use(express.static('public'))
app.get('/login',(req,res)=>{
    res.sendFile(__dirname+'/public/login.html')
})
/*
app.get('/usermanager', (req, res) = {
    //...
})*/