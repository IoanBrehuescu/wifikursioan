const express = require('express')
const r = express.Router();

const controller = require('./controller')// lade ich die controller

r.get('/', controller.htmlUI);
r.post('/', controller.saveUser);
r.get('/delete/:id', controller.deleteUser);
r.post('/login', controller.checkLogin);



//prima line de code
module.exports = r; //wir exportieren Routing für /usermanager