const model = require('./model');
const view = require('./view');

let controller = {
    htmlUI: async (req, res) => {
        model.getUsers((users) => {
            let tabelle = view.generateTable(users);
            view.renderHTML(res, tabelle)
        })

    },//server beantworten,
    saveUser: (req, res) => {
        model.addUser(req.body.uname,req.body.pwd,()=> {
            controller.htmlUI(req,res);
        })

    },
    deleteUser:(req,res)=>{
        model.deleteUser(req.params.id,()=>{
            // controller.htmlUI(req,res);
            res.redirect('/usermanager')
        })
    },
    checkLogin:(req,res)=>{
        model.userExists(req.body.username,req.body.passwort,(ok)=>{
            res.send(ok ? 'Login OK':'Username od. Passwort falsch');
        });
    }
}
module.exports = controller;