const fs = require('fs');
const { v4: uuidv4 } = require('uuid');
const controller = require('./controller');
const bcrypt = require('bcrypt')
//arbeitet mit dem daten JSON SCV...
let model = {
    getUsers:(callback)=>{
        fs.readFile(__dirname+'/model/users.json',(err,data)=>{
            let users = JSON.parse(data)
           callback(users) 
        });
    },
    saveUsers:(users,callback)=>{
        fs.writeFile(__dirname+'/model/users.json',JSON.stringify(users),
        (err)=>{
            callback()
        })
    },
    addUser:(username,passwort,callback)=>{
        let gefunden = user.find(user=>user.username==username)
        if(!gefunden){
                bcrypt.compare(p,gefunden.passwort,(err,result)=>{
                    callback(result);
                })
            }else{
                callback(false);
            }

        
        model.getUsers(users=>{
            bcrypt.hash(passwort,10,(err,hashPasswort)=>{
                let neuerUser ={id:uuidv4(),username,passwort:hashPasswort}
                users.push(neuerUser)
                model.saveUsers(users,callback);
            })
        });
    },
    deleteUser:(id,callback)=>{
        model.getUsers((users)=>{
            users = users.filter(user => user.id != id)
            model.saveUsers(users,callback);
        })
    },
   userExists:(u,p,callback)=>{
    model.getUsers((users)=>{
        let gefunden=users.find(user => user.username == u)
        if(gefunden){
            bcrypt.compare(p,gefunden.passwort,(err,result)=>{
                callback(result);
            })
        }else{
            callback(false);
        }
    })
   }
    
}
module.exports = model;