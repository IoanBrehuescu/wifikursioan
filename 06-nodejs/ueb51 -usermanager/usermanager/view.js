const fs = require('fs')
const model = require('./model');

let view = {
    renderHTML: (res, utabelle) => {
        fs.readFile(__dirname + '/view/users.html', (err, data) => {

            let html = data.toString();
            html = html.replace('<!-- usertabelle -->', utabelle)

            res.end(html)


        });

    },
    generateTable: (users) => {
        return `<table border="1">
        <tr><th>ID</th><th>Username</th><th>Passwort</th></tr>
        
        ${
            users.map(user => `<tr>
            <td>${user.id}</td>
            <td>${user.username}</td>
            <td>${user.passwort}</td>
            <td><a href="/usermanager/delete/${user.id}">löschen</a></td>
            </tr>`).join('')
            
        }
        </table>`;
    }
}
module.exports=view;