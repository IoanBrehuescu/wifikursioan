console.log('extern');

let f = 2;

let multi = (a,b) => {
    return a*b*f;
} 

// module export ist nicht VanillaJS sondern NodeJS
// immer ein Objekt exportieren
module.exports = {
    multipliziere : multi,
    multi
}
