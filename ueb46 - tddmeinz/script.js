/*
    Anforderung: 
    Komponente/Funktion, die eine Zahl mit 2 multipliziert und das Ergebnis zurückgibt
    Funktionsname: "multiplikation2"
*/
let multiplikation2 = (x) => x * 2;

/*
    Anforderung: 
    Komponente/Funktion name "add", soll zwei Zahlen addieren
    - beide Werte müssen Zahl sein oder Zahl als String (auch erlaubt)
    - wenn Buchstabe übergeben wird, dann soll 0 als Ergebnis aufscheinen
    - Kommazahlen sind auch als String mit , möglich
    - wenn zweite Parameter fehlt, Ergebnis ebenfalls 0
*/
let add = function (a, b) {
    if (typeof a == 'string') {
       a = a.replace(',', '.')
    }
    if (typeof b == 'string') {
       b = b.replace(',', '.')
       
    }
    a = parseFloat(a);
    b = parseFloat(b);

    if (isNaN(a) || isNaN(b)) {
        return 0;
    }

    return a + b;


}

let addHTML = (a,b)=>{
    let summe = add(a,b);
    document.querySelector('#ausgabe').innerHTML = summe;
}

let asyncAdd = (a,b, cb=()=>{} )=>{
    setTimeout(()=>{
        cb(add(a,b) );
    },1000)
}