/**
 * ermittelt eine Zufallszahl (GANZE Zahl)
 * @param {number} min - kleinste mögliche Zahl
 * @param {number} max - größte mögliche Zahl
 * @returns 
 */
let zufall = (min,max) => {
    return Math.floor(Math.random() * (max-min+1) ) + min;
}

module.exports = {
    zufall
}