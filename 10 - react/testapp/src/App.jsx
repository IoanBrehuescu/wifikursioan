import './App.css';
import TodoForm from './TodoForm'
import TodoList from './TodoList'
import { useState, useEffect } from 'react';// useEffect -> beim async im react

function App() {

  // const todos =[
  //   "Ordner anlegen",
  //   "npx create-react-app NAME",
  //   "Ordner speichern",
  //   "npx create-react-app asa"
  // ]
  const [todos, setTodos] = useState([
    // { t: "Ordner anlegen", c: false },
    // { t: "npx create-react-app NAME", c: true },
  ])
  const fetchData = () => {
    fetch('http://localhost:3001/todo')
    .then(r=>r.json())
    .then(d=>{
      setTodos(d)
    })
  }

  useEffect(() => {
    fetchData();
  }, [])

  const addTodo = (text) => {
    // todos.push(text)
    // console.log(todos)
    // let todoKopie = [...todos];
    // todoKopie.push({ t: text, c: false })
    // setTodos(todoKopie)
    // setTodos([...todos, text]);
    fetch('http://localhost:3001/todo',{
      method:'post',
      headers:{'Content-Type':'application/json'},
      body:JSON.stringify({text:text})
    }).then(r=>{
      fetchData();
    })
  }
  const checkTodo = (i) => {
    // let todoKopie = [...todos];
    // todoKopie[i].c = !todoKopie[i].c
    // setTodos(todoKopie);
    fetch('http://localhost:3001/todo/'+i,{
      method:'PATCH',
      headers:{'Content-Type':'application/json'},
      body:JSON.stringify({c:!todos[i].c})
    }).then(r=>{
    fetchData()
    })
  }
  return (<>
    <TodoForm addTodo={addTodo} />
    <TodoList todos={todos} checkTodo={checkTodo} />

  </>)

}



export default App;
