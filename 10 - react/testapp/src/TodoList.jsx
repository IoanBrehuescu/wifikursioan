import './TodoList.css';
import Todo from './Todo';

function TodoList(props) {

    console.log(props.todos)

    return (
        <ul id="myUL">
            {
                props.todos.map((todo, i) => (
                    <Todo
                        text={todo.t}
                        checked={todo.c}
                        checkTodo={props.checkTodo}
                        i={i} />
                ))
            }
        </ul>);
}
export default TodoList;