import './TodoForm.css';
import { useRef } from 'react'

function TodoForm(props) {
    const textImput = useRef(null);
    const clickHandler = () => {
        let v = textImput.current.value;
        props.addTodo(v)
        console.log(props.todos);
    }
    return (
        <div id="myDIV" className="header">
            <h2>Meine TODO-Liste</h2>
            <input
                type="text"
                id="myInput"
                placeholder="Title..."
                ref={textImput} />
            <span className="addBtn"
                onClick={clickHandler}>+</span>
        </div>
    );
}
export default TodoForm;