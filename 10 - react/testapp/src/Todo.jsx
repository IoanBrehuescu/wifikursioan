import './Todo.css';

function Todo(props) {
    

    return (
        <li
            className={props.checked ? 'checked' : ''}
            onClick={()=>{props.checkTodo(props.i)}}
        >
            {props.text}
            <span className='close'>{"\u00d7"}</span>
        </li>
    );
}
export default Todo;