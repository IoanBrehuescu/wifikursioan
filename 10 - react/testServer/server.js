const express = require('express');
const app = express()
const cors = require('cors')
const{MongoClient,ObjectId}=require('mongodb')
const client = new MongoClient('mongodb+srv://Wifitest:60rXQ8be1dA1j6AE@todo.jghabjl.mongodb.net/?retryWrites=true&w=majority')
client.connect()
    .then(() => {
        console.log('DB connected.');
        app.listen(3001, () => {
            console.log('Server läuft');
        });
    })
    .catch((e) => {
        console.log('DB connection err:', e);
    });

app.use(cors({methods:['GET','POST','PATCH','DELETE']}));
app.use(express.json());

let todos = [
    { t: "Aufgabe1", c: true },
    { t: "Aufgabe2", c: true },
    { t: "Aufgabe3", c: false },
    { t: "Aufgabe4", c: false }
];

app.get('/todo', (req, res) => {
    res.send(todos)
})
app.post('/todo',async (req, res) => {
    let db = client.db ('Wifitest')
    let todoCollection = db.collection('todo')
    await todoCollection.insertOne({t:req.body.text,c:false})

    todos.push({ t: req.body.text, c: false })
    res.status(204).end('')//204 => OK, aber kein Inhalt
})
app.patch('/todo/:id',async (req, res) => {
    let db = client.db ('Wifitest')
    let todoCollection = db.collection('todo')
    await todoCollection.insertOne({t:req.body.text,c:false})
    todos[req.params.id * 1].c = req.body.c === true
    res.status(204).end('')//204 => OK, aber kein Inhalt
})
app.delete('/todo/:id', (req, res) => {
    todos = todos.filter((t, i) => i != req.params.id);
    res.status(204).end('')//204 => OK, aber kein Inhalt
})