//MAIN-Procces
//von dort wird RENDERER-Process (Browser-Fenster)gestartet
// IPC => Komunikation zwischen Main un Render
// im Main wird ipcMain benötigt, im Render ipcRenderer
// Komunikation ist eventdriven
const { app, BrowserWindow, screen, Menu, shell, ipcMain } = require('electron')
let fenster;
const appMenu = [
    {label:"",submenu:[
        
       
    ]},
    {label:"Meine App",submenu:[
        {label:"Beenden",click:()=>{
            app.quit();
        },
    accelerator:"Cmd+Q"},
        {label:'DevTools',role:'toggleDevTools'},
        {label:"Beep",click:shell.beep,accelerator:"Cmd+J"}
    ]},
    {label:"Funktionen",submenu:[
        {label:"Startseite",click:()=>{
            fenster.loadFile('view/index.html')
        }},
        {label:"Seite2",click:()=>{
            fenster.loadFile('view/seite2.html')
        }},
        {label:'WIFI Wien',click:()=>{
            shell.openExternal('https://wifi-wien.at')
        }}
    ]},
   
]


const starteApplikation = () => {
let displays = screen.getAllDisplays();
let externalDisplay = displays[displays.length - 1].workArea;
console.log(displays);




//Aufgabe: Fenster öffnet am "letzten" Bildschirm mit Breite 400 und voller Höhe am rechten

     fenster = new BrowserWindow({
        width:400,
        height:externalDisplay.height,
        x:externalDisplay.x + externalDisplay.width - 400,
        y:externalDisplay.y,
        resizable:false,
        movable:false,
        webPreferences:{
            devTools:true,
            nodeIntegration:true,//geht aber macht man nicht!
            contextIsolation:false
        },
        icon: __dirname + '/assets/icon_128.png'
        /*frame : false,
        transparent:true,
        alwaysOnTop:true*/
        
    });
    fenster.loadFile('view/index.html')
    fenster.webContents.openDevTools(); 
// Alle Render haben appMenu
    Menu.setApplicationMenu(Menu.buildFromTemplate(appMenu))
}
app.whenReady().then(starteApplikation);


ipcMain.on('ende',()=>{
    app.quit();
})