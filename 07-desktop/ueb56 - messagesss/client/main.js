const { app, BrowserWindow, ipcMain } = require('electron');

let fenster;
const starteApp = () => {
    fenster = new BrowserWindow({
        width: 1100,
        height: 900,
        x: 1920,
        y: 100,
        autoHideMenuBar: true,
        webPreferences: {
            preload: __dirname + '/view/preload.js'
        },
        icon: __dirname + '/assets/icon.png'
    });
    fenster.loadFile('view/login.html');
}

app.whenReady().then(() => {
    starteApp();
});
let eingeloggteUser = '';
ipcMain.on('login', (e, username) => {
    eingeloggteUser = username;
});
ipcMain.on('load',(e)=>{
    e.reply('eingeloggt',eingeloggteUser);
})
