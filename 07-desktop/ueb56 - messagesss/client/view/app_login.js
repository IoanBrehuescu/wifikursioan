const LIVE = false;
const SERVER = LIVE ? '' : 'http://localhost:8888';

const submitLogin = (e) => {
    e.preventDefault();
    let username = document.querySelector('#benutzername').value;
    let passwort = document.querySelector('#passwort').value;
    fetch(SERVER + '/login', {
        method: 'post',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({ username, passwort })
    })
        .then(response => response.json())
        .then(data => {
            if (data.token != '') {
                sessionStorage.setItem('jwt', data.token); // JWT lokal speichern, (localStorage, sessionStorage, cookie oder. Main-Prozess) 
                //username an main schiken
                window.chatAPI.loginUser(username)
                location.href = 'chat.html';
            } else {
                document.querySelector('#msg').innerHTML = `<p>Kein User gefunden od. Passwort falsch.</p>`;
            }

        });
}
if (document.querySelector('#einloggen')) {
    document.querySelector('#einloggen').addEventListener('click', submitLogin);
}


const submitRegister = (e) => {
    e.preventDefault();
    let username = document.querySelector('#benutzername').value;
    let pwd = document.querySelector('#passwort').value;
    let vn = document.querySelector('#vorname').value;
    let nn = document.querySelector('#nachname').value;
    let avatar = document.querySelector('#avatar').value;

    fetch(SERVER + '/register', {
        method: 'post',
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
            username,
            vorname: vn,
            nachname: nn,
            avatar,
            passwort: pwd
        })
    })
        .then(response => response.json())
        .then(data => {
            if (data.ok) {
                location.href = 'login.html';
            } else {
                document.querySelector('#msg').innerHTML = `<p>User bereits vorhanden</p>`;
            }

        }).catch(e => {
            document.querySelector('#msg').innerHTML = `<p>Error Daten fehlerhaft.</p>`;
        });
}
if (document.querySelector('#register')) {
    document.querySelector('#register').addEventListener('click', submitRegister);
}
