const { contextBridge, ipcRenderer } = require( 'electron' );
contextBridge.exposeInMainWorld( 'chatAPI', {
    loginUser:(username)=>ipcRenderer.send('login',username),
    loadUser:()=>ipcRenderer.send('load'),
    eingeloggtAls:(callback)=>ipcRenderer.on('eingeloggt',callback)

});
