console.log( 'session', sessionStorage.getItem( 'jwt' ) );
let token = sessionStorage.getItem( 'jwt' );

window.chatAPI.loadUser(); 
window.chatAPI.eingeloggtAls( (e,username)=>{
    document.querySelector( '#userloggedin' ).innerHTML = username;
    loadUserList();
   
});

document.querySelector( '#logout' ).addEventListener( 'click', ()=>{
    sessionStorage.removeItem( 'jwt' );
    window.chatAPI.loginUser( '' );
});
let aktiverUser;
const loadChatData=(event)=>{
    aktiverUser = event.target.getAtribut('data-chat')
    fetch(SERVER+'/chat/'+aktiverUser, {
        method: 'GET',
        headers:{'Authorization':'Bearer '+ token},
        
    })
    .then(response=>response.json())
    .then(data =>{
        document.querySelector('.chat').innerHTML=''
        data.forEach((chat)=>{
            let div = document.createElement('div');
            div.innerHTML=chat.nachricht;
            div.classlist.add('bubble')
            
        })
    })
}

// load Userliste
const loadUserList = ()=>{
    fetch( 'http://localhost:9003/users', {
        method:'get',
        headers:{'Authorization':'Bearer '+token}
    } ).then( response => response.json() )
    .then( data => {
        console.log( data );
        let list = document.querySelector( '.people' );
        list.innerHTML = '';
        data.forEach( el=>{
            let li = `<li class="person" data-chat="${el.username}">
            <img src="${el.avatar}" alt="" />
            <span class="name">${el.vorname} ${el.nachname}</span>
            <span class="time"></span>
            <span class="preview">@${el.username}</span>
        </li>`;
            list.innerHTML += li;
        });
        document.querySelectorAll('.person').forEach((el)=>{
            el.addEventListener
        })
    });
}



/*
* Method: POST
* URL: /chat/:user_id
* Auth-Header Bearer
* Content-Type: application/json   
* Body: nachricht
* Response: [ Nachrichten ]  */