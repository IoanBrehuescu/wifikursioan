# Messages
- https://codepen.io/Momciloo/pen/bEdbxY

## Server
- Benutzer verwalten
- Chatverläufe verwalten
- CRUD-Webservice / WebAPI => Beschreibung (1.)
- Auth-Service (Login, Register), JWT als Auth-Bearer
- kein UI, kein HTML !!! nur API => URL, die was tun

## Client
- Electron Basic/Window
- HTML-UI
- Daten abrufen/senden (WebAPI)

## API Doc
- Registrierung
    * Method: POST
    * URL: /register
    * Content-Type: application/json
    * Body: vorname, nachname, username, passwort, avatar
    * Response: {ok:true} false wenn User bereits existiert

- Login (check Username/Passwort)
    * Method: POST
    * URL: /login
    * Content-Type: application/json   
    * Body: username, passwort
    * Response: {token:STRING} leer wenn falsch

- Userliste (alle)
    * Method: GET
    * URL: /users
    * Auth-Header Bearer
    * Body: empty
    * Response: [ Userobjekten ]

- Chatverlauf (ein User)
    * Method: GET
    * URL: /chat/:user_id
    * Auth-Header Bearer
    * Body: empty
    * Response: [ Nachrichten ]

- Nachricht schreiben
    * Method: POST
    * URL: /chat/:user_id
    * Auth-Header Bearer
    * Content-Type: application/json   
    * Body: nachricht
    * Response: [ Nachrichten ]    