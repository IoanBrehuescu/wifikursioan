// Module
const express = require('express');
const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const JWT_SECRET = 'klh213498zishcvnb24qp9ghalshbvlhsabei5z';

// Express-Server
const app = express();
const PORT = 9003;
const server = app.listen(PORT, () => {
    console.log(`Server http://localhost:${PORT}`);
});
const USERS = __dirname + '/model/users.json';
const CHATS = __dirname + '/model/chat.json'


// Middleware
app.use(express.json());

// Routes
// Registrieren (kein TOKEN)
app.post('/register', (req, res) => {
    console.log('POST /register', req.body);
    fs.readFile(USERS, async (err, data) => {
        if (!err) {
            try {
                let users = JSON.parse(data);
                if (!req.body.vorname || !req.body.nachname || !req.body.username || !req.body.passwort || !req.body.avatar) {
                    throw new Error('');
                }
                // Duplikate erkennen
                let gefundenerUser = users.find((el, i) => el.username === req.body.username);
                if (gefundenerUser) {
                    res.status(200).send({ ok: false });
                } else {
                    req.body.passwort = await bcrypt.hash(req.body.passwort, 10);
                    users.push(req.body);
                    fs.writeFile(USERS, JSON.stringify(users, null, "\t"), (err) => {
                        if (!err) {
                            res.status(200).send({ ok: true });
                        } else {
                            res.status(500).send('writeFile Error');
                        }
                    });
                }
            } catch (e) {
                res.status(500).send('user JSON parse Error oder Daten Error');
            }
        } else {
            res.status(500).send('readFile Error');
        }
    })

});

// Login (kein TOKEN)
app.post('/login', (req, res) => {
    console.log('POST /login', req.body);
    fs.readFile(USERS, async (err, data) => {
        if (!err) {
            try {
                let users = JSON.parse(data);
                let gefundenerUser = users.find((el, i) => el.username === req.body.username);
                if (gefundenerUser && await bcrypt.compare(req.body.passwort, gefundenerUser.passwort)) {
                    // Loginlogik => Session  
                    const token = jwt.sign({ username: req.body.username }, JWT_SECRET, { expiresIn: "2h" });


                    res.status(200).send({ token: token });
                } else {
                    res.status(200).send({ token: '' });
                }
            } catch (e) {
                console.log(e);
                res.status(500).send('user JSON parse Error');
            }
        } else {
            res.status(500).send('readFile Error');
        }
    })
});

// Middleware !!
const verifyToken = (req, res, next) => {
    const bearer = req.headers['authorization'];
    if (typeof bearer !== 'undefined') {
        let token = bearer.replace('Bearer ', '');
        jwt.verify(token, JWT_SECRET, (err, decoded) => {
            if (!err) {
                req.user = decoded.username; // später nutzen
                next();
            } else {
                res.status(401).end('');
            }
        });
    } else {
        res.status(401).end('');
    }
}

// für alle Routes darunter
// app.use( verifyToken );
app.get('/users', verifyToken, (req, res) => {
    // console.log( decoded ); // undefined
    // console.log( req.user ); // gesetzt
    fs.readFile(USERS, async (err, data) => {
        let users = JSON.parse(data);
        users = users.map(el => {
            delete el.passwort;
            return el;
            /*
            return {
                vorname: el.vorname,
                nachname: el.nachname,
                username: el.username,
                avatar: el.avatar
            }
            */
        });
        users = users.filter(el => el.username != req.user); // nur User wo Username nicht gleich eingeloggter User ist
        res.send(users);
    });
});
const filterChats = (chats, req) => {
    return chats.filter(el => (el.von == req.params.username && el.zu == req.user) ||
        (el.von == req.user && el.zu == req.params.username))
}

app.get('/chat/:username', verifyToken, (req, res) => {
    fs.readFile(CHATS, (err, data) => {
        let chats = JSON.parse(data);

        chats = filterChats(chats, req)
        res.send(chats);
    })
});
app.post('/chat/:username', verifyToken, (req, res) => {
    fs.readFile(CHATS, (err, data) => {
        let chats = JSON.parse(data);
        let jetzt = new Date();
        let datetime = `${jetzt.getDate()}.${jetzt.getMonth() + 1}.${jetzt.getFullYear()}.${jetzt.getHours()}}`;
        let neue = {
            von: req.user,
            zu: req.params.username,
            nachricht: req.body.nachricht,
            datetime: Date.now()
        };
        chats.push(neue);
        fs.writeFile(CHATS, JSON.stringify(chats), (err) => {
            res.send(chats);
        })
    })
})
