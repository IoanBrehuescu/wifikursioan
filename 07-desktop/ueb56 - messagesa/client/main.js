const {app,BrowserWindow} = require( 'electron' );

let win;
const starteApp = () => {
    win = new BrowserWindow({
        width: 1200,
        height: 900,
        x: 2000,
        y: 100,
        resizable: false,
        movable: true,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            
        },


    })
    win.loadFile('view/register.html');
    win.webContents.openDevTools();
    
}
app.whenReady().then(starteApp);