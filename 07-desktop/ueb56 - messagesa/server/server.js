// Module
const express = require( 'express' );
const fs = require( 'fs' );
const jwt = require( 'jsonwebtoken' );
const bcrypt = require( 'bcrypt' );

const JWT_SECRET = 'klh213498zishcvnb24qp9ghalshbvlhsabei5z';

// Express-Server
const app = express();
const PORT = 9003;
const server = app.listen( PORT, ()=>{
    console.log( `Server http://localhost:${PORT}` );
});
const USERS = __dirname + '/model/users.json';


// Middleware
app.use( express.json() ); 

// Routes
// Registrieren (kein TOKEN)
app.post( '/register', (req,res)=>{
    console.log( 'POST /register', req.body );
    fs.readFile( USERS, async (err,data)=>{
        if (!err ) {          
            try {
                let users = JSON.parse( data );
                if ( !req.body.vorname || !req.body.nachname || !req.body.username || !req.body.passwort || !req.body.avatar ) {
                    throw new Error( '' );
                }
                // Duplikate erkennen
                let gefundenerUser = users.find( (el,i)=>el.username === req.body.username );
                if ( gefundenerUser ) {    
                    res.status(200).send({ok:false}); 
                } else {
                    req.body.passwort = await bcrypt.hash( req.body.passwort, 10);
                    users.push( req.body );
                    fs.writeFile( USERS, JSON.stringify( users, null, "\t" ), (err)=>{
                        if ( !err ) {
                            res.status(200).send({ok:true}); 
                        } else {
                            res.status(500).send('writeFile Error');
                        }
                    });
                }
            } catch(e) {
                res.status(500).send('user JSON parse Error oder Daten Error'); 
            }           
        } else {
            res.status(500).send('readFile Error');
        }
    })

});

// Login (kein TOKEN)
app.post( '/login', (req,res)=>{
    console.log( 'POST /login', req.body );
    fs.readFile( USERS, async(err,data)=>{
        if (!err ) {          
            try {
                let users = JSON.parse( data );
                let gefundenerUser = users.find( (el,i)=>el.username === req.body.username );
                if ( gefundenerUser && await bcrypt.compare( req.body.passwort, gefundenerUser.passwort ) ) {  
                    // Loginlogik => Session  
                    const token = jwt.sign({username:req.body.username},JWT_SECRET,{expiresIn:"2h"});
                    res.status(200).send({token:token}); 
                } else {
                    res.status(200).send({token:''});
                }
            } catch(e) {
                console.log( e );
                res.status(500).send('user JSON parse Error');
            }           
        } else {
            res.status(500).send('readFile Error');
        }
    })
});

