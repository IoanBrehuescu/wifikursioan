const { app, BrowserWindow, screen,ipcMain } = require('electron'); 
const { loadTodos, saveTodos } = require('./model/todos');
const starteApplikation = () => {
    let displays = screen.getAllDisplays();
    let externalDisplay = displays[displays.length - 1].workArea;
    console.log(displays);
    
    let fenster;
    
    
    //Aufgabe: Fenster öffnet am "letzten" Bildschirm mit Breite 400 und voller Höhe am rechten
    
         fenster = new BrowserWindow({
            width:400,
            height:externalDisplay.height,
            x:externalDisplay.x + externalDisplay.width - 400,
            y:externalDisplay.y,
            resizable:false,
            movable:false,
            webPreferences:{
                autoHideMenuBar:true,
                devTools:true,
                nodeIntegration:true,//geht aber macht man nicht!
                contextIsolation:false
            },
            icon: __dirname + '/assets/icon_128.png'
            /*frame : false,
            transparent:true,
            alwaysOnTop:true*/
            
        });
        fenster.loadFile('view/index.html')
        fenster.webContents.openDevTools(); 
    // Alle Render haben appMenu
        
    }
    app.whenReady().then(starteApplikation);
    
    // let todos = []





ipcMain.on('toggleTodoStatus', async (event, index) => {
    if (index >= 0 && index < todos.length) {
        todos[index].erledigt = !todos[index].erledigt;
        await todosUpdated(todos);
    }
});

ipcMain.on('neu', async (event, text) => {
    let todos = await loadTodos();
    const newsTodo = {
        text: text,
        erledigt: false
    };
    todos.push(newsTodo);
   await saveTodos(todos)
   event.reply('todos',todos)
});
ipcMain.on('laden', async (event) => {
    let todos = await loadTodos();
   // e.reply('todos',todos)
   fenster.send('todos',todos)
});

ipcMain.on('deleteTodo', async (event, index) => {
    if (index >= 0 && index < todos.length) {
        todos.splice(index, 1);
        await todoDelete(todos);
    }
});
    ipcMain.on('ende',()=>{
        app.quit();
    })
    


