// MAIN Prozess
// 600x400 index.html

const { app, BrowserWindow, Menu, ipcMain, dialog } = require("electron");
//import {app, BrowserWindow} from 'electron'
const fs = require('fs');
let win;
const appMenu = [
    {
        label: "Bild Editor", submenu: [
            {
                label: "Beenden"
                , click: () => { app.quit(); }
            },
            {
                label: "Tuwas"
                , click: () => { win.webContents.send('tuwas') }
            }
        ]
    },

]

const starteApp = () => {
    win = new BrowserWindow({
        width: 400,
        height: 600,
        x: 2000,
        y: 100,
        resizable: false,
        icon: __dirname + '/assets/icon.png',
        movable: true,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: false,
            contextIsolation: true,
            preload: __dirname + '/view/preload.js'
        },


    })
    win.loadFile('view/index.html');
    win.webContents.openDevTools();
    Menu.setApplicationMenu(Menu.buildFromTemplate(appMenu))
}
ipcMain.on('open', async (e) => {
    console.log('Bild öffnen'); //Terminal !!!
    let bild = await dialog.showOpenDialog(win, {
        properties: ['openFile'],
        filters: [
            { name: 'Bilder', extensions: ['png', 'jpg', 'jpeg'] }
        ]
    });
    if (!bild.canceled) {
        console.log(bild)
        // win.loadFile(bild.filePaths[0]);
        // fs.readFile(bild.filePaths[0]......)
        // e.reply('zeigbild', bild.filePaths[0]);
        win.webContents.send('zeigebild', bild.filePaths[0]);
    }

    ipcMain.on('save', async (e, data) => {
        console.log(data)
        data = data.replace('data:image/png;base64,', '')
        let pfad = await dialog.showSaveDialog(win, { defaultPath: 'test.png' });
        if (!pfad.canceled) {
            await fs.promises.writeFile(pfad.filePath , data, 'base64');
        }

    })
});
app.whenReady().then(starteApp);