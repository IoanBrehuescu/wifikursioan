//const {ipcRenderer} = require('electron'); // nur wenn nodeIntegration: true
console.log('Hallo Welt')


document.querySelector('#open').addEventListener('click', () => {
    //ipcRenderer.send('tuwas');
    window.bildAPI.bildOeffnen();
});


window.bildAPI.zeigeBild((e, pfad) => {
    erzeugeBild(pfad);
})

const erzeugeBild = (pfad) => {
    let bild = document.createElement('img')
    bild.src = pfad;
    bild.onload = () => {
        console.log(bild.width, bild.height);
        let canvas = document.createElement('canvas')
        canvas.width = bild.width;
        canvas.height = bild.height;
        document.querySelector('body').appendChild(canvas);
        let ctx = canvas.getContext('2d')
        ctx.drawImage(bild, 0, 0);
    }

}
document.querySelector('#flip').addEventListener('click', () => {
    let canvas = document.querySelector('canvas');
    let ctx = canvas.getContext('2d')
    ctx.scale(-1, 1);
    ctx.drawImage(canvas, -canvas.width, 0);// fügt aktuelles sichtbares bild wieder ein
    ctx.scale(-1, 1);//reset normal,damit es beim nächsten klick wieder funktioniert
})

document.querySelector('#save').addEventListener('click', () => {

    let canvas = document.querySelector('canvas')
    let inhaltCanvas = canvas.toDataURL();
    window.bildAPI.speichereBild(inhaltCanvas);
})