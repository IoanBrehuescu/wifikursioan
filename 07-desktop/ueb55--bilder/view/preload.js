const { contextBridge, ipcRenderer } = require('electron');

contextBridge.exposeInMainWorld('bildAPI', {
    bildOeffnen: () => {
        ipcRenderer.send('open');
    },
    zeigeBild: (callback) => {

        ipcRenderer.on('zeigebild', callback)
    },
    speichereBild: (data) => {
        ipcRenderer.send('save', data)
    }
});



