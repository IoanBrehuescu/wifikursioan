const {ipcRenderer} = require( 'electron' );

const erzeugeListe = ( todos )=>{
    // let todos = [];
    let ul = document.querySelector( '#myUL' );
    ul.innerHTML = '';
    for ( let i = 0; i<todos.length; i++ ) {
        let li = document.createElement( 'li' );
        li.innerHTML = todos[i].text;
        ul.appendChild( li );
        if ( todos[i].erledigt ) {
            li.classList.add( 'checked' );
        }
        // löschen 
        let span = document.createElement( 'span' );
        span.classList.add( 'close' );
        span.innerHTML = "\u00D7";
        li.appendChild( span );

        li.addEventListener( 'click', ()=>{
           ipcRenderer.send( 'check', i );
        })

        span.addEventListener( 'click', ( e )=>{
            e.stopPropagation(); // verhindert Bubbling, es wird jetzt nicht mehr der Klick bei <li> ausgelöst
            ipcRenderer.send( 'delete', i );
        })

    }
}

const neuesTodo = ()=>{
    let text = document.querySelector( '#myInput' ).value.trim();
    if ( text != '' ) {
        ipcRenderer.send('neu',text);
        document.querySelector( '#myInput' ).value = '';
        //ladeTodos();
    }
}

document.querySelector( '.addBtn' ).addEventListener( 'click', neuesTodo );

//ladeTodos();
ipcRenderer.send( 'laden' );
ipcRenderer.on( 'todos', (e,todos)=>{
    erzeugeListe( todos );
});