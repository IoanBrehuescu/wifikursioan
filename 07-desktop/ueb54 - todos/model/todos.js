const fs = require( 'fs' )
const FILE = __dirname + '/todos.json';

const loadTodos = async()=>{
    return JSON.parse( await fs.promises.readFile( FILE ) );
}

const saveTodos = async( todos )=>{
    await fs.promises.writeFile( FILE, JSON.stringify(todos,null,"\t") );
}

module.exports = {
    loadTodos,
    saveTodos
}