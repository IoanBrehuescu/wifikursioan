const {app, BrowserWindow, ipcMain} = require( 'electron' );
const {loadTodos,saveTodos} = require( './model/todos.js' );

let meinFenster;

let starteApplikation = () => {
    meinFenster = new BrowserWindow({
        width:400,
        height:600,
        resizable: false,
        movable: true,
        autoHideMenuBar: true,
        webPreferences:{
            nodeIntegration: true, 
            contextIsolation: false,
            devTools: true
        },
        frame:true,       
        icon: __dirname + '/build/to-do-list.png'
    });
    meinFenster.loadFile( 'view/index.html' );   

    meinFenster.webContents.openDevTools();

}

app.whenReady().then( starteApplikation );

ipcMain.on( 'neu', async(e,text) => {
    let todos = await loadTodos();
    let neuesTodo = {text,erledigt:false};
    todos.push( neuesTodo );
    await saveTodos( todos );
    e.reply( 'todos', todos );
});

ipcMain.on( 'laden', async( e )=>{
    let todos = await loadTodos();
    //e.reply( 'todos', todos );
    meinFenster.send( 'todos', todos );
});

ipcMain.on( 'check', async(e,i)=>{
    let todos = await loadTodos();
    todos[i].erledigt = !todos[i].erledigt;
    await saveTodos( todos );
    e.reply( 'todos', todos );
});

ipcMain.on( 'delete', async(e,i)=>{
    let todos = await loadTodos();
    todos.splice(i,1);
    await saveTodos( todos );
    e.reply( 'todos', todos );
});