const express = require('express');
const fs = require('fs');
const app = express();
const PORT = process.env.PORT || 7001;

const server = app.listen(PORT, () => {
    console.log(`Server http://localhost:${PORT}`);
});

// Middleware
app.use(express.static('public'));

app.get("/", function(req,res){
    res.sendFile(__dirname + "/index.html")
});
app.get("/data",(req,res)=>{
    console.log(req.query.file);
    fs.readFile(__dirname + '/'+ req.query.file,(err,data)=>{
        console.log(data.toString)
        data = data.toString();
        let zeilen = data.split("\n\r")
        zeilen.splice(0,1)
        for(let i = 0;i<zeilen.length;i++){
            let daten=zeilen[i].split(';');
            zeilen[i] = {
                url:daten[0],
                title:daten[1],
                eigenes:daten[2]
            }
        }
        setTimeout(()=>{
            res.send(zeilen)
        },2000)
        
    })


})
